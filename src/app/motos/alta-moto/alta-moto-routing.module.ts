import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AltaMotoPage } from './alta-moto.page';

const routes: Routes = [
  {
    path: '',
    component: AltaMotoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AltaMotoPageRoutingModule {}
