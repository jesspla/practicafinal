import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import {
  Capacitor,
  Plugins,
  CameraResultType,
  CameraPhoto,
  CameraSource,
} from '@capacitor/core';
import { NavController } from '@ionic/angular';
import { Moto } from 'src/app/models/moto';
import { MotoService } from 'src/app/services/motos.service';
const { Camera} = Plugins;


@Component({
  selector: 'app-alta-moto',
  templateUrl: './alta-moto.page.html',
  styleUrls: ['./alta-moto.page.scss'],
})
export class AltaMotoPage implements OnInit {

  altaMoto: FormGroup
  image: any = undefined
  originalPhoto: CameraPhoto

  constructor(private fb: FormBuilder, private motoService: MotoService, private navController: NavController) { }

  ngOnInit() {
    this.altaMoto = this.fb.group({
      marca: ['', [Validators.required]],
      modelo: ['', [Validators.required]],
      year: ['', [Validators.required]],
      precio: ['', [Validators.required]]
    })
  }

  changeListener($event): void {
    this.image = $event.target.files[0];
  }

  async takePicture() {
    this.originalPhoto = await Camera.getPhoto({
      quality: 30,
      allowEditing: false,
      resultType: CameraResultType.DataUrl,
    });
  }

  saveMoto() {

    //this.image = (<HTMLInputElement>document.getElementsByName("foto")[0]).files[0];
    const moto: Moto = {
      marca: this.altaMoto.get("marca").value,
      modelo: this.altaMoto.get("modelo").value,
      year: this.altaMoto.get("year").value,
      precio: this.altaMoto.get("precio").value,
      foto: this.originalPhoto ? this.originalPhoto.dataUrl : this.image
    }

    

    this.motoService.saveMoto(moto).then(res => {
      console.log(res)
      this.navController.back()
    })
  }

}
