import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { Moto } from 'src/app/models/moto';
import { MotoService } from 'src/app/services/motos.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  moto: Moto

  constructor(
    private route: ActivatedRoute, 
    private motoService: MotoService, 
    private alertController: AlertController, 
    private navController: NavController,
    private router: Router) { }

  async ngOnInit() {
    const motoId = Number(this.route.snapshot.paramMap.get("id"))
    const motos: Moto[] = this.motoService.getMotos()
    this.moto = motos.find((moto: Moto) => moto.id == motoId)
  }

  async deleteMoto() {
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Cuidado!',
        message: '¿Estás seguro que deseas eliminar?',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Vale',
            handler: () => {
              this.motoService.deleteMoto(this.moto.id).then(data => {
                console.log(data)
                this.navController.back()
                //this.router.navigate(["/home"])
              })
            }
          }
        ]
      });
  
      await alert.present();    
  }

}
