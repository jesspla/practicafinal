import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MotosPage } from './motos.page';

const routes: Routes = [
  {
    path: '',
    component: MotosPage
  },
  {
    path: 'marca/:marca',
    component: MotosPage
  },
  {
    path: 'detail',
    loadChildren: () => import('./detail/detail.module').then( m => m.DetailPageModule)
  },
  {
    path: 'alta-moto',
    loadChildren: () => import('./alta-moto/alta-moto.module').then( m => m.AltaMotoPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule {}
