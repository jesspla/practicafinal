import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MotoService } from '../services/motos.service';
import { Moto } from '../models/moto';

@Component({
  selector: 'app-motos',
  templateUrl: './motos.page.html',
  styleUrls: ['./motos.page.scss']
})
export class MotosPage{
  public marca: string;
  public motos: Moto[];

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly motoService: MotoService,
    private router: Router
  ) {}

  ionViewDidEnter(): void {
    this.marca = this.activatedRoute.snapshot.paramMap.get('marca');

    this.motoService.getMotosFromDb(this.marca).then(motos => {
      this.motos = motos;
      this.motoService.setMotos(motos);
    });
  }

  goToMoto(moto: Moto) {
    this.router.navigate(['/motos/detail/', moto.id])
  }

  altaMoto() {
    this.router.navigate(['/motos/alta-moto']);
  }
}
