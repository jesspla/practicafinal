import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Moto } from '../models/moto';

@Injectable({providedIn: 'root'})
export class MotoService {
    motos: Moto[] = []

    constructor() { }

    getMotosFromDb(marca: string = ""): Promise<Moto[]> {
        let getMotosURL = environment.apiURL + '/motos'
        if (marca !== null && marca !== "") {
            getMotosURL += `/?marca=${marca}`
        }

        return fetch(getMotosURL)
        .then((respuesta) => {
          return respuesta.json();
        })
    }

    getMotos() {
        return this.motos
    }

    setMotos(motos: Moto[]) {
        this.motos = motos
    }

    deleteMoto(id: number) {
        return fetch(environment.apiURL + '/motos/' + id, {
            method: 'DELETE',
        }).then((respuesta) => {
            return respuesta.text();
        })
    }

    saveMoto(moto: Moto) {

        let formData = new FormData();

        formData.append('marca', moto.marca)
        formData.append('modelo', moto.modelo)
        formData.append('year', moto.year)
        formData.append('precio', moto.precio)
        formData.append('foto', moto.foto)

        console.log(formData.get('marca'))

        return fetch(environment.apiURL + '/motos', {
            method: 'POST',
            mode: 'cors',
            body: JSON.stringify(moto),
            headers:{
              'Access-Control-Allow-Origin': '*',
              'Content-Type': 'application/json'
            }
          }).then(res => {
              return res.text()
            })
          .catch(error => {
              console.error('Error:', error)
            })
    }
    
}